-- MariaDB dump 10.19-11.3.2-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: gong_operator_cake
-- ------------------------------------------------------
-- Server version	11.3.2-MariaDB-1:11.3.2+maria~ubu2204

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES
(1,'Cafe Ohne','','','','','','Emerkingen'),
(2,'Hemperium','','','','','','Ulm'),
(3,'Rössle','','','','','','Sontheim'),
(4,'Beetle Bar','','','','','89584','Ehingen'),
(5,'Club Schili','','','','','','Ulm');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT '/media/members',
  `instruments` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES
(1,'Jürgen','Schleicher','Joxe','/media/members','2,3'),
(2,'Louis','Kienle','','/media/members','0'),
(3,'Daniel','Ott','Otti','/media/members','1,3'),
(4,'Johannes','Braun','','/media/members','0,3');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phinxlog`
--

LOCK TABLES `phinxlog` WRITE;
/*!40000 ALTER TABLE `phinxlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `phinxlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_apps`
--

DROP TABLE IF EXISTS `rhino_apps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_apps` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) NOT NULL,
  `overview_fields` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `rhino_group_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_apps`
--

LOCK TABLES `rhino_apps` WRITE;
/*!40000 ALTER TABLE `rhino_apps` DISABLE KEYS */;
INSERT INTO `rhino_apps` VALUES
(1,'shows','Shows',NULL,1,1,'2024-02-26 21:08:33','2024-02-26 21:08:33'),
(2,'locations','Locations',NULL,1,1,'2024-02-26 21:25:58','2024-02-26 21:25:58'),
(3,'songs','Songs','',1,1,'2024-02-26 21:36:48','2024-02-26 21:36:54'),
(4,'tracks','Tracks',NULL,1,1,'2024-02-26 21:40:50','2024-02-26 21:40:50'),
(5,'setlists','Setlisten',NULL,1,1,'2024-02-26 21:42:31','2024-02-26 21:42:31'),
(6,'members','Members',NULL,1,1,'2024-02-26 21:42:47','2024-02-26 21:42:47');
/*!40000 ALTER TABLE `rhino_apps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_contents`
--

DROP TABLE IF EXISTS `rhino_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_contents` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `element_id` int(11) DEFAULT NULL,
  `html` text DEFAULT NULL,
  `media` varchar(255) DEFAULT NULL,
  `widget` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `position` int(11) DEFAULT 0,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_contents`
--

LOCK TABLES `rhino_contents` WRITE;
/*!40000 ALTER TABLE `rhino_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `rhino_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_fields`
--

DROP TABLE IF EXISTS `rhino_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_fields` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `table_name` varchar(100) NOT NULL,
  `type` varchar(255) DEFAULT 'string',
  `description` text DEFAULT NULL,
  `default_value` varchar(255) DEFAULT NULL,
  `settings` text DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_fields`
--

LOCK TABLES `rhino_fields` WRITE;
/*!40000 ALTER TABLE `rhino_fields` DISABLE KEYS */;
INSERT INTO `rhino_fields` VALUES
(1,'datetime','Datum & Uhrzeit','shows','dateTime','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:09:27','2024-02-26 21:10:34'),
(2,'description','Beschreibung','shows','text','',NULL,'{\"showEditor\":\"\"}','2024-02-26 21:10:49','2024-02-26 21:10:49'),
(3,'location_id','Location','shows','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"0\",\"selectSeparator\":\",\",\"selectKeys\":\"\",\"selectValues\":\"\",\"selectFromTable\":\"locations\",\"selectFromValue\":\"id\",\"selectFromAlias\":\"name\",\"selectFromSQL\":\"{\\\"orderBy\\\": {\\\"name\\\": \\\"ASC\\\"}}\"}','2024-02-26 21:25:40','2024-02-26 21:31:26'),
(4,'name','Name','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:26:09','2024-02-26 21:26:09'),
(5,'description','Beschreibung','locations','text','',NULL,'{\"showEditor\":\"\"}','2024-02-26 21:26:23','2024-02-26 21:26:23'),
(6,'latitude','Latitude','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:26:38','2024-02-26 21:26:38'),
(7,'longitude','Longitude','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:26:56','2024-02-26 21:26:56'),
(8,'street_address','Anschrift','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:27:16','2024-02-26 21:27:16'),
(9,'zip','Postleitzahl','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:27:28','2024-02-26 21:27:28'),
(10,'city','Stadt','locations','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:27:35','2024-02-26 21:27:35'),
(11,'title','Titel','songs','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:37:15','2024-02-26 21:37:15'),
(12,'lyrics','Text','songs','text','',NULL,'{\"showEditor\":\"\"}','2024-02-26 21:37:26','2024-02-26 21:37:26'),
(13,'status','Status','songs','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"0\",\"selectSeparator\":\",\",\"selectKeys\":\"draft\\r\\nactive\\r\\narchive\",\"selectValues\":\"Entwurf\\r\\nAktiv\\r\\nArchiviert\",\"selectFromTable\":\"\",\"selectFromValue\":\"\",\"selectFromAlias\":\"\",\"selectFromSQL\":\"\"}','2024-02-26 21:39:20','2024-02-26 21:39:20'),
(14,'type','Typ','songs','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"0\",\"selectSeparator\":\",\",\"selectKeys\":\"original\\r\\ncover\",\"selectValues\":\"Original\\r\\nCover\",\"selectFromTable\":\"\",\"selectFromValue\":\"\",\"selectFromAlias\":\"\",\"selectFromSQL\":\"\"}','2024-02-26 21:40:09','2024-02-26 21:40:09'),
(15,'song_id','Song','tracks','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"0\",\"selectSeparator\":\",\",\"selectKeys\":\"\",\"selectValues\":\"\",\"selectFromTable\":\"songs\",\"selectFromValue\":\"id\",\"selectFromAlias\":\"title\",\"selectFromSQL\":\"{\\\"orderBy\\\": {\\\"position\\\": \\\"ASC\\\"}}\"}','2024-02-26 21:41:25','2024-02-26 21:41:25'),
(16,'file','Datei','tracks','upload','',NULL,'{\"uploadDirectory\":\"\",\"uploadTypes\":\"\",\"uploadOverwrite\":\"\",\"uploadMultiple\":\"\"}','2024-02-26 21:41:55','2024-02-26 21:41:55'),
(17,'url','URL','tracks','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:42:03','2024-02-26 21:42:03'),
(18,'firstname','Vorname','members','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:43:08','2024-02-26 21:43:08'),
(19,'lastname','Nachname','members','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:43:16','2024-02-26 21:43:16'),
(20,'nickname','Nickname','members','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:43:30','2024-02-26 21:43:30'),
(21,'image','Bild','members','upload','',NULL,'{\"uploadDirectory\":\"\",\"uploadTypes\":\"\",\"uploadOverwrite\":\"\",\"uploadMultiple\":\"\"}','2024-02-26 21:44:08','2024-02-26 21:44:08'),
(22,'instruments','Instrumente','members','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"1\",\"selectSeparator\":\",\",\"selectKeys\":\"git\\r\\nbass\\r\\ndrums\\r\\nvocals\",\"selectValues\":\"GItarre\\r\\nBass\\r\\nSchlagzeug\\r\\nGesang\",\"selectFromTable\":\"\",\"selectFromValue\":\"\",\"selectFromAlias\":\"\",\"selectFromSQL\":\"\"}','2024-02-26 21:45:07','2024-02-26 21:45:07'),
(23,'title','Titel','setlists','string','',NULL,'{\"autosuggest\":\"\"}','2024-02-26 21:46:13','2024-02-26 21:46:13'),
(24,'setlist_id','Setlist','shows','select','',NULL,'{\"selectEmpty\":\"0\",\"selectMultiple\":\"0\",\"selectSeparator\":\",\",\"selectKeys\":\"\",\"selectValues\":\"\",\"selectFromTable\":\"setlists\",\"selectFromValue\":\"id\",\"selectFromAlias\":\"title\",\"selectFromSQL\":\"\"}','2024-02-26 21:46:43','2024-02-26 21:46:43');
/*!40000 ALTER TABLE `rhino_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_groups`
--

DROP TABLE IF EXISTS `rhino_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_groups`
--

LOCK TABLES `rhino_groups` WRITE;
/*!40000 ALTER TABLE `rhino_groups` DISABLE KEYS */;
INSERT INTO `rhino_groups` VALUES
(1,'Gong Operator',1,'2024-02-26 21:08:21','2024-02-26 21:08:21');
/*!40000 ALTER TABLE `rhino_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_media`
--

DROP TABLE IF EXISTS `rhino_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `media_category_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_media`
--

LOCK TABLES `rhino_media` WRITE;
/*!40000 ALTER TABLE `rhino_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `rhino_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_media_categories`
--

DROP TABLE IF EXISTS `rhino_media_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_media_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_media_categories`
--

LOCK TABLES `rhino_media_categories` WRITE;
/*!40000 ALTER TABLE `rhino_media_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `rhino_media_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_nodes`
--

DROP TABLE IF EXISTS `rhino_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_nodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(11) NOT NULL,
  `node_type` int(11) NOT NULL,
  `role` int(11) DEFAULT 0,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(10) NOT NULL,
  `rght` int(10) NOT NULL,
  `level` int(10) NOT NULL DEFAULT 0,
  `template_id` int(11) DEFAULT 0,
  `language` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `config` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lft` (`lft`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_nodes`
--

LOCK TABLES `rhino_nodes` WRITE;
/*!40000 ALTER TABLE `rhino_nodes` DISABLE KEYS */;
INSERT INTO `rhino_nodes` VALUES
(1,'Home',1,'2024-02-24 19:37:44','2024-02-24 19:37:44',1,0,3,NULL,0,2,0,1,NULL,NULL,NULL,NULL),
(2,'content',1,'2024-02-24 19:37:44','2024-02-24 19:37:44',1,1,0,1,1,1,1,2,NULL,NULL,NULL,'{\"time\":1690121834854,\"blocks\":[{\"id\":\"BkMrFh55lD\",\"type\":\"header\",\"data\":{\"text\":\"Welcome to Rhino &#x1F98F;\",\"level\":1}},{\"id\":\"R_LcFT6kwI\",\"type\":\"paragraph\",\"data\":{\"text\":\"The fast but stable Application-Framwork.<br>Powered by <a href=\\\"https://cakephp.org/\\\">CakePHP</a>.\"}}],\"version\":\"2.26.5\"}');
/*!40000 ALTER TABLE `rhino_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_phinxlog`
--

DROP TABLE IF EXISTS `rhino_phinxlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_phinxlog`
--

LOCK TABLES `rhino_phinxlog` WRITE;
/*!40000 ALTER TABLE `rhino_phinxlog` DISABLE KEYS */;
INSERT INTO `rhino_phinxlog` VALUES
(20230411070244,'RhinoInit','2024-02-24 19:37:44','2024-02-24 19:37:44',0),
(20240104123228,'PageTree','2024-02-24 19:37:44','2024-02-24 19:37:44',0);
/*!40000 ALTER TABLE `rhino_phinxlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_roles`
--

DROP TABLE IF EXISTS `rhino_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `access` text NOT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_roles`
--

LOCK TABLES `rhino_roles` WRITE;
/*!40000 ALTER TABLE `rhino_roles` DISABLE KEYS */;
INSERT INTO `rhino_roles` VALUES
(1,'Admin','','2024-02-24 19:37:44','2024-02-24 19:37:44'),
(2,'Redakteur','','2024-02-24 19:37:44','2024-02-24 19:37:44'),
(3,'User','','2024-02-24 19:37:44','2024-02-24 19:37:44');
/*!40000 ALTER TABLE `rhino_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_templates`
--

DROP TABLE IF EXISTS `rhino_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `template_type` int(11) NOT NULL DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_templates`
--

LOCK TABLES `rhino_templates` WRITE;
/*!40000 ALTER TABLE `rhino_templates` DISABLE KEYS */;
INSERT INTO `rhino_templates` VALUES
(1,'Default','default.php',1,0,'2024-02-24 19:37:44','2024-02-24 19:37:44'),
(2,'Text','text.php',1,1,'2024-02-24 19:37:44','2024-02-24 19:37:44');
/*!40000 ALTER TABLE `rhino_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_users`
--

DROP TABLE IF EXISTS `rhino_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role_id` varchar(100) NOT NULL DEFAULT '1',
  `theme` varchar(255) NOT NULL DEFAULT 'rhino',
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT 1,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_users`
--

LOCK TABLES `rhino_users` WRITE;
/*!40000 ALTER TABLE `rhino_users` DISABLE KEYS */;
INSERT INTO `rhino_users` VALUES
(1,'hannenz','hannenz@posteo.de','1','rhino','$2y$10$32edxq/gMw83s2wty0tCmO1flW.HKHBenFxythgw5kaXvZfmQc5mm',1,'2024-02-24 19:39:30','2024-02-24 19:39:30');
/*!40000 ALTER TABLE `rhino_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_widget_categories`
--

DROP TABLE IF EXISTS `rhino_widget_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_widget_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_widget_categories`
--

LOCK TABLES `rhino_widget_categories` WRITE;
/*!40000 ALTER TABLE `rhino_widget_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `rhino_widget_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rhino_widgets`
--

DROP TABLE IF EXISTS `rhino_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rhino_widgets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `widget_category_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT current_timestamp(),
  `modified` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rhino_widgets`
--

LOCK TABLES `rhino_widgets` WRITE;
/*!40000 ALTER TABLE `rhino_widgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `rhino_widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setlists`
--

DROP TABLE IF EXISTS `setlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setlists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setlists`
--

LOCK TABLES `setlists` WRITE;
/*!40000 ALTER TABLE `setlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `setlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shows`
--

DROP TABLE IF EXISTS `shows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shows` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `description` text DEFAULT NULL,
  `location_id` varchar(255) DEFAULT NULL,
  `setlist_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shows`
--

LOCK TABLES `shows` WRITE;
/*!40000 ALTER TABLE `shows` DISABLE KEYS */;
INSERT INTO `shows` VALUES
(1,'2024-04-27 20:00:00','','1',NULL),
(2,'2022-12-15 20:00:00',' Weihnachtspogo mit Rampenbüro','2',NULL),
(3,'2023-02-25 20:00:00','+ Agent Shit','3',NULL),
(4,'2023-04-29 20:00:00','','1',NULL),
(5,'2023-05-13 20:00:00','+ Mental Freeze','4',NULL),
(6,'2023-10-06 20:00:00','+ Go Go Gazelle','5',NULL),
(7,'2023-12-08 20:00:00','Weihnachtspogo mit Rampenbüro und Agent Shit ','2',NULL);
/*!40000 ALTER TABLE `shows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `lyrics` text DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES
(1,'Anti','','1','0'),
(2,'Space Rock Zombies','','1','0'),
(3,'Something That Detonates','','1','0'),
(4,'Rock\'n\'Roll Rocket','','1','0'),
(5,'Too Late','','1','0'),
(6,'Rock\'n\'Roll Maniac','','1','0'),
(7,'Search And Destroy','','1','1'),
(8,'I Should Do','','1','0'),
(9,'Gong Operator','','1','0'),
(10,'Chicago \'73','','1','0'),
(11,'Fast Life (In Forbidden Frontiers)','','1','0'),
(12,'Dead Of Me','','2','1');
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracks`
--

DROP TABLE IF EXISTS `tracks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `song_id` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT 'media/tracks',
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracks`
--

LOCK TABLES `tracks` WRITE;
/*!40000 ALTER TABLE `tracks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tracks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-03 21:39:45
