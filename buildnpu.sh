#!/bin/bash
set -e

TAG=0.0.2
TOKEN=86b4894de12b9de05e05ecd596f2ba38f5b0eae0 

docker build \
	-t codeberg.org/hannenz/gong-operator-website:$TAG \
	-t codeberg.org/hannenz/gong-operator-website:latest \
	-f docker/prod/Dockerfile \
	.

printf "\nPush? [y/N]\n"
read answer

if [[ ${answer} == "y" ]] ; then 
	docker login -u hannenz -p $TOKEN codeberg.org
	docker push codeberg.org/hannenz/gong-operator-website:$TAG
	docker push codeberg.org/hannenz/gong-operator-website:latest
	docker logout
fi 
printf "Success.\n"
