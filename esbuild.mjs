'use strict';

import path from "node:path";
import fs from 'node:fs';
import os from "node:os";
import esbuild from 'esbuild';
import { sassPlugin } from 'esbuild-sass-plugin';
import postcss from 'postcss';
import autoprefixer from 'autoprefixer';
import clear from 'esbuild-plugin-clear';
import imagemin from 'imagemin';
import imageminSvgo from 'imagemin-svgo';
import imageminOptipng from 'imagemin-optipng';
import moment from 'moment';
import * as git from "git-rev-sync";
import log from 'fancy-log';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import browserSync from "@rbnlffl/esbuild-plugin-browser-sync";
import { exit } from "node:process";

const pkg = JSON.parse(fs.readFileSync('./package.json'));

let banner = [
	"/**",
	" * @project       " + pkg.name,
	" * @author        " + pkg.author,
	" * @build         " + moment().format("llll") + " ET",
	" * @copyright     Copyright (c) " + moment().format("YYYY") + ", " + pkg.copyright,
	" *",
	" */",
	""
].join("\n");

try {
	banner = [
		"/**",
		" * @project       " + pkg.name,
		" * @author        " + pkg.author,
		" * @build         " + moment().format("llll") + " ET",
		" * @release       " + git.long('./') + " [" + git.branch('./') + "]",
		" * @copyright     Copyright (c) " + moment().format("YYYY") + ", " + pkg.copyright,
		" *",
		" */",
		""
	].join("\n");
} catch (error) {
	log('Banner with git info could not be build');
}

const argv = yargs(hideBin(process.argv)).argv
const isProduction = (argv.production !== undefined);
const isWatching = (argv.watch !== undefined);
const src = pkg.project_settings.source;
const dist = pkg.project_settings.prefix;
const root = path.dirname('');
const modules = path.resolve(root, "node_modules") + '/';

let plugins = [
	sassPlugin({
		loadPaths: getFromModules(pkg.project_settings.scssIncludes, []).concat([
			src + 'css',
			'node_modules/'
		]),
		precision: 3,
		errLogToConsole: true,
		prefer: 'sass',
		quietDeps: true,
		async transform(source) {
			const { css } = await postcss([autoprefixer]).process(source, {
				from: undefined,
			});
			return css;
		},
		importMapper: (path) => path.replace(/^settings\//, './app/css/settings'),
		precompile(source, pathname) {
			const basedir = path.dirname(pathname)
			return source.replace(/(url\(['"]?)(\.\.?\/)([^'")]+['"]?\))/g, `$1${basedir}/$2$3`)
		}
	})
];

if (isWatching) {
	plugins.push(browserSync({
		proxy: pkg.project_settings.browser_sync_proxy ??
			"https://" +
			pkg.name +
			".localhost",
		open: false, // Don't open browser, change to "local" if you want or see https://browsersync.io/docs/options#option-open
		notify: false, // Don't notify on every change
		https: {
			key: os.homedir() + "/server.key",
			cert: os.homedir() + "/server.crt",
		},
	}))
} else {
	plugins.unshift(clear(dist + 'css/'));
	plugins.unshift(clear(dist + 'js/'));
	plugins.unshift(clear(dist + 'icon/'));
	plugins.unshift(clear(dist + 'img/'));
	plugins.unshift(clear(dist + 'assets/'));
}

const buildOptions = {
	entryPoints: getFromPackage(pkg.project_settings.entryPoints, [
		// Add more in the package.json under "project_settings"
		// -- OR --
		// 'src + "js/main.js",'
		// You can add more files here that will be built seperately,
	]),
	banner: {
		js: banner,
		css: banner,
	},
	bundle: true,                   // Bundle dependencies into the output
	outdir: 'app/webroot',
	assetNames: 'assets/[name]-[hash]',
	sourcemap: !isProduction,                // Generate sourcemaps
	minify: isProduction,
	platform: 'browser',
	target: 'esnext',               // Set the target JavaScript version (e.g., esnext, es6)
	loader: {
		'.ts': 'ts',  // Make sure esbuild processes TypeScript files
		'.scss': 'css',
		'.png': 'copy',
		'.woff': 'copy',
		'.woff2': 'copy',
		'.eot': 'copy',
		'.ttf': 'copy',
		'.svg': 'copy',
	},
	resolveExtensions: [
		'.js',
		'.scss'
	],
	external: [
		'/img/*',
		'/icon/*'
	],
	plugins: plugins
};

const build = () => {
	esbuild.build(buildOptions)
		.then(() => log("⚡ Build complete! ⚡"))
		.catch(() => process.exit(1));
}

const forDirs = (source, callback) =>
	fs.readdir(source, { withFileTypes: true }, (err, files) => {
		if (err) {
			callback(err)
		} else {
			files
				.filter(dirent => dirent.isDirectory())
				.map(dirent => dirent.name)
				.forEach(dir => callback(dir));
		}
	});

const images = async () => {
	log('Optimizing Images');

	const svgoOptions = {
		plugins: [
			{
				name: 'preset-default',
				params: {
					overrides: {
						removeViewBox: false,
						mergePaths: false,
						removeUnknownsAndDefaults: false,
						cleanupAttrs: false,
						inlineStyles: false
					},
				},
			},
		]
	};

	await imagemin([src + 'icon/**/*']
		.concat(pkg.project_settings.iconSrc.map((path) => modules + path)),
		{
			destination: dist + 'icon',
			plugins: [
				imageminSvgo(svgoOptions)
			]
		}
	);

	await imagemin([
		src + 'img/*',
	], {
		destination: dist + 'img',
		plugins: [
			imageminOptipng({ optimizationLevel: 5 }),
		]
	});

	forDirs(src + 'img', async (dir) => {
		let source = src + 'img/' + dir + '/*';
		let destination = dist + 'img/' + dir;

		await imagemin([
			source,
		], {
			destination: destination,
			plugins: [
				imageminOptipng({ optimizationLevel: 5 }),
			]
		});

		log(`Images optimized in ${dir}`);
	});

	log('Images optimized');
}

const watch = async () => {
	let ctx = await esbuild.context(buildOptions)
		.catch(() => process.exit(1));
	await ctx.watch();
}

if (isWatching) {
	log("Started Watch");
	watch();
	log("Watching...");
} else {
	log("Building App" + ((isProduction) ? " [production build]" : " [development build]"));
	build();
	images();
}

function getFromPackage(packageData, sourceData) {
	return sourceData.concat(
		packageData?.map((data) => src + data)
	);
}

function getFromModules(packageData, sourceData) {
	const data = sourceData.concat(packageData);
	return data.map((path) => modules + path);
}
