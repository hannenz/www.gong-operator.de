export default class AlbumPlayer extends HTMLElement {

	constructor() {
		super();
		this.attachShadow({mode: 'open'});
		this.audios = [];

		this.playing = false;
		this.currentTrack = null;
		this.currentTrackNr = -1;
		const template = document.getElementById('album-player-template');
		this.shadowRoot.appendChild(template.content.cloneNode(true));

		this.albumPlayerEl = this.shadowRoot.querySelector('.album-player');
		this.coverEl = this.shadowRoot.querySelector('.album-player__cover');
		this.tracklistEl = this.shadowRoot.querySelector('.album-player__tracklist');
		this.controlsEl = this.shadowRoot.querySelector('.album-player__controls');
		this.playBtn = this.shadowRoot.querySelector('.album-player__btn--play');
		this.prevBtn = this.shadowRoot.querySelector('.album-player__btn--prev');
		this.nextBtn = this.shadowRoot.querySelector('.album-player__btn--next');
		this.progress = this.shadowRoot.querySelector('.album-player__progress');

		this.tracks = document.querySelectorAll(`audio[rel=${this.dataset.rel}]`);

		this.playBtn.onclick = e => this.onPlayBtnClicked(e);
		this.prevBtn.onclick = e => this.onSkipBtnClicked(e, -1);
		this.nextBtn.onclick = e => this.onSkipBtnClicked(e, 1);

		this.image = this.shadowRoot.querySelector('img');
		this.image.src = this.dataset.cover;

		this.tracks.forEach(track => {
			
			const btn = document.createElement('button');
			btn.innerHTML = track.title;
			const li = document.createElement('li');
			li.appendChild(btn);
			this.tracklistEl.appendChild(li);

			this.audios.push(track);
			track.addEventListener('ended', (ev) => this.onSkipBtnClicked(ev, 1));
			li.addEventListener('click', ev => this.onTrackClicked(ev, track));
		});

		this.ti = window.setInterval(() => this.updateTime(), 50);
		this.progress.addEventListener('change', e => this.onProgressChanged()(e));
	}



	playTrack(nr) {
		if (nr < 0 || nr >= this.audios.length) {
			return;
		}
		if (this.currentTrack != null) {
			this.currentTrack.pause();
		}
		this.currentTrackNr = nr;
		this.currentTrack = this.audios[this.currentTrackNr];

		this.currentTrack.currentTime = 0;
		this.progress.value = 0;

		this.currentTrack.play();
		this.playing = true;

		this.tracklistEl.childNodes.forEach(li => li.classList.remove('current'));
		const li = this.tracklistEl.querySelector(`li:nth-child(${nr + 1})`);
		li.classList.add('current');
		this.albumPlayerEl.toggleAttribute('playing', this.playing);
	}


	onTrackClicked(event, track) {
		const index = this.audios.findIndex((el, index, arr) => {
			return (el === track);
		});
		this.playTrack(index);
	}


	updateTime() {
		if (this.currentTrack === null) {
			return;
		}

		const t = this.currentTrack.currentTime;
		const total = this.currentTrack.duration;
		const v = (t / total * 1000).toFixed(0).toString()
		this.progress.value = v;
	}


	onProgressChanged() {
		if (this.currentTrack === null) {
			return;
		}
		
		const time = this.currentTrack.duration * (this.progress.value / 1000);
		this.currentTrack.currentTime = time;
	}


	onPlayBtnClicked(event) {
		if (this.currentTrack === null) {
			this.playTrack(0);
			return;
		}

		if (this.playing == false) {
			this.playing = true;
			this.currentTrack.play();
		}
		else {
			this.playing = false;
			this.currentTrack.pause();
		}
		this.albumPlayerEl.toggleAttribute('playing', this.playing);
	}


	onSkipBtnClicked(event, direction) {
		if (this.currentTrack === null) {
			return;
		}

		this.currentTrackNr += direction;
		if (this.currentTrackNr >= this.audios.length) {
			this.currentTrackNr = this.audios.length - 1;
		}
		else if (this.currentTrackNr < 0) {
			this.currentTrackNr = 0;
		}

		this.playTrack(this.currentTrackNr);
	}
}



customElements.define('album-player', AlbumPlayer);
