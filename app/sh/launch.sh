#!/usr/bin/env bash
set -xe
/var/www/html/bin/cake migrations migrate
echo "************ Launching Web Server ************"
apache2-foreground
