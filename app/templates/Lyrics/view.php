<article>

    <header>
        <h2 class="headline"><?= $file ?></h2>
        <?php if ($isCached): ?>
            <div>Is cached&nbsp;
            <?php echo $this->Html->link('reload w/out cache', ['action' => 'clearCache']); ?>
            </div>
        <?php else: ?>
            <div>Is not cached</div>
        <?php endif ?>
    </header>


    <div>
        <?= $html; ?>
    </div>


    <footer>
        <?= $this->Html->link(_('Back'), ['action' => 'index']); ?>
    </footer>
</article>
