<h2>Index</h2>
<?php if ($isCached): ?>
    <div>Is cached&nbsp;
    <?php echo $this->Html->link('reload w/out cache', ['action' => 'clearCache']); ?>
    </div>
<?php else: ?>
    <div>Is not cached</div>
<?php endif ?>
<ul>
<?php foreach ($files as $file): ?>
    <li><?php echo $this->Html->link($file, ['controller' => 'Lyrics', 'action' => 'view', $file]); ?></li>
<?php endforeach ?>
</ul>
