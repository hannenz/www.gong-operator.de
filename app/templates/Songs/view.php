<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Song $song
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Song'), ['action' => 'edit', $song->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Song'), ['action' => 'delete', $song->id], ['confirm' => __('Are you sure you want to delete # {0}?', $song->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Songs'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Song'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="songs view content">
            <h3><?= h($song->title) ?></h3>
            <table>
                <tr>
                    <th><?= __('Title') ?></th>
                    <td><?= h($song->title) ?></td>
                </tr>
                <tr>
                    <th><?= __('Status') ?></th>
                    <td><?= h($song->status) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($song->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($song->id) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Lyrics') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($song->lyrics)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Tracks') ?></h4>
                <?php if (!empty($song->tracks)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Song Id') ?></th>
                            <th><?= __('File') ?></th>
                            <th><?= __('Url') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($song->tracks as $track) : ?>
                        <tr>
                            <td><?= h($track->id) ?></td>
                            <td><?= h($track->song_id) ?></td>
                            <td><?= h($track->file) ?></td>
                            <td><?= h($track->url) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Tracks', 'action' => 'view', $track->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Tracks', 'action' => 'edit', $track->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tracks', 'action' => 'delete', $track->id], ['confirm' => __('Are you sure you want to delete # {0}?', $track->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>