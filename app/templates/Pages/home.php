                    <?= $this->element('separator', ['stars' => 3]); ?>
                    <h2 class="headline">Listen</h2>

					<album-player data-rel="st" data-cover="/img/Gong_Operator_Cover.webp"></album-player>

					<audio rel="st" title="Rock'n'Roll Maniac" src="/media/rocknroll_maniac.m4a"></audio>
					<audio rel="st" title="Gong Operator" src="/media/gong_operator.m4a"></audio>
					<audio rel="st" title="Too Late" src="/media/too_late.m4a"></audio>
					<audio rel="st" title="Rock'n'Roll Rocket" src="/media/rocknroll_rocket.m4a"></audio>
					<audio rel="st" title="Chicago&apos;73" src="/media/chicago_73.m4a"></audio>

					<template id="album-player-template">
						<link rel="stylesheet" href="/css/albumplayer.css">
						<div class="album-player">
							<picture class="album-player__cover"><img /></picture>
							<ol class="album-player__tracklist"></ol>
							<input value="0" min="0" max="1000" type="range" class="album-player__progress">
							<menu class="album-player__controls">
								<li class="album-player__control"><button class="album-player__btn album-player__btn--prev" title="Previous Track">⏮</button></li>
								<li class="album-player__control"><button class="album-player__btn album-player__btn--play" title="Play/Pause">⏵</button></li>
								<li class="album-player__control"><button class="album-player__btn album-player__btn--next" title="Next Track">⏭</button></li>
							</menu>
						</div>
					</template>



                    <figure class="album">
                        <!--img src="/img/Gong_Operator_Cover.webp" alt="Gong Operator Album Cover"-->
                        <figcaption>
                            <p>Listen on</p>
                            <ul class="streamings">
                                <li><?= $this->Html->link('Deezer', '#'); ?></li>
                                <li><?= $this->Html->link('Spotify', '#'); ?></li>
                                <li><?= $this->Html->link('SoundCloud', '#'); ?></li>
                                <li><?= $this->Html->link('YouTube', '#'); ?></li>
                            </ul>
                        </figcaption>
                    </figure>

                    <?= $this->element('separator', ['stars' => 3]); ?>

                    <h2 class="headline">Live</h2>

                    <?php $cell = $this->cell('UpcomingShows');  echo $cell ?>

                    <?php //echo $this->element('partials/main'); ?>

                    <?= $this->element('separator', ['stars' => 3]); ?>

                    <h2 class="headline">Past Shows</h2>
                    <?php $cell = $this->cell('PastShows');  echo $cell ?>

                    <?= $this->element('separator', ['stars' => 3]); ?>


