<section>
    <h2 class="headline">Upcoming Shows</h2>
    <ul>
    <?php foreach ($shows as $show): ?>
        <li>
            <p><?= $show->datetime->nice(); ?></p>
            <h3><?= $show->location->name; ?></h3>
        </li>
    <?php endforeach ?>
    </ul>
</section>

