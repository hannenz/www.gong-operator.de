<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Show $show
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Show'), ['action' => 'edit', $show->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Show'), ['action' => 'delete', $show->id], ['confirm' => __('Are you sure you want to delete # {0}?', $show->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Shows'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Show'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="shows view content">
            <h3><?= h($show->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Location') ?></th>
                    <td><?= $show->hasValue('location') ? $this->Html->link($show->location->name, ['controller' => 'Locations', 'action' => 'view', $show->location->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Setlist Id') ?></th>
                    <td><?= h($show->setlist_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($show->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Datetime') ?></th>
                    <td><?= h($show->datetime) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($show->description)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
