<ul class="gigs gigs--upcoming stack">
    <?php foreach ($shows as $show): ?>
        <li>
            <dl class="gig">
                <dt><?= $show->datetime->i18nFormat('dd/MMM/YYYY'); ?></dt>
                <dd><?= $show->location->name; ?> <?= $show->location->city; ?></dd>
            </dl>
            <?php if (!empty($show->description)): ?>
            <div>
                <?= $show->description ?>
            </div>
            <?php endif ?>
        </li>
    <?php endforeach ?>
</ul>
