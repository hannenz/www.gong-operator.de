<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setlist $setlist
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $setlist->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $setlist->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Setlists'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="setlists form content">
            <?= $this->Form->create($setlist) ?>
            <fieldset>
                <legend><?= __('Edit Setlist') ?></legend>
                <?php
                    echo $this->Form->control('title');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
