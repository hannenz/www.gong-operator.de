<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Setlist $setlist
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Setlist'), ['action' => 'edit', $setlist->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Setlist'), ['action' => 'delete', $setlist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $setlist->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Setlists'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Setlist'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="setlists view content">
            <h3><?= h($setlist->title) ?></h3>
            <table>
                <tr>
                    <th><?= __('Title') ?></th>
                    <td><?= h($setlist->title) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($setlist->id) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Shows') ?></h4>
                <?php if (!empty($setlist->shows)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Datetime') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Location Id') ?></th>
                            <th><?= __('Setlist Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($setlist->shows as $shows) : ?>
                        <tr>
                            <td><?= h($shows->id) ?></td>
                            <td><?= h($shows->datetime) ?></td>
                            <td><?= h($shows->description) ?></td>
                            <td><?= h($shows->location_id) ?></td>
                            <td><?= h($shows->setlist_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Shows', 'action' => 'view', $shows->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Shows', 'action' => 'edit', $shows->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Shows', 'action' => 'delete', $shows->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shows->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
