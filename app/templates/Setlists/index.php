<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Setlist> $setlists
 */
?>
<div class="setlists index content">
    <?= $this->Html->link(__('New Setlist'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Setlists') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('title') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($setlists as $setlist): ?>
                <tr>
                    <td><?= $this->Number->format($setlist->id) ?></td>
                    <td><?= h($setlist->title) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $setlist->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $setlist->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $setlist->id], ['confirm' => __('Are you sure you want to delete # {0}?', $setlist->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
