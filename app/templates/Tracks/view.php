<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Track $track
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Track'), ['action' => 'edit', $track->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Track'), ['action' => 'delete', $track->id], ['confirm' => __('Are you sure you want to delete # {0}?', $track->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Tracks'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Track'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="tracks view content">
            <h3><?= h($track->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Song') ?></th>
                    <td><?= $track->hasValue('song') ? $this->Html->link($track->song->title, ['controller' => 'Songs', 'action' => 'view', $track->song->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('File') ?></th>
                    <td><?= h($track->file) ?></td>
                </tr>
                <tr>
                    <th><?= __('Url') ?></th>
                    <td><?= h($track->url) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($track->id) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
