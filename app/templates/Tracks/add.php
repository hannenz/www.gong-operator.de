<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Track $track
 * @var \Cake\Collection\CollectionInterface|string[] $songs
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Tracks'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column column-80">
        <div class="tracks form content">
            <?= $this->Form->create($track) ?>
            <fieldset>
                <legend><?= __('Add Track') ?></legend>
                <?php
                    echo $this->Form->control('song_id', ['options' => $songs, 'empty' => true]);
                    echo $this->Form->control('file');
                    echo $this->Form->control('url');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
