<!doctype html>
<html class="no-js" lang="<?= \Cake\Core\Configure::read('App.defaultLocale') ?>">

    <head>
        <?= $this->element('partials/head') ?>
    </head>

    <?= $this->fetch('Rhino') ?>

    <body id="home">
        <?= $this->Html->link('#main', _('Skip navigation'), ['class' => 'visually-hidden']); ?>


        <div class="wrapper noisy-background">
            <img class="bg-photo" src="/img/band-2024.webp">
            <div id="shape" class="shape"></div>
            <div class="inner-wrapper">
                <header class="main-header">
                    <h1 class="brand">
                        <div class="brand__image octopus">
                            <?= file_get_contents(WWW_ROOT . '/img/OP_Logo.svg'); ?>
                        </div>
                    </h1>
                </header>

                <main id="main" class="main-content stack stack--full">

                    <?= $this->element('separator', ['stars' => 3]); ?>
                    <h2 class="headline">Listen</h2>

					<album-player data-rel="st" data-cover="/img/Gong_Operator_Cover.webp"></album-player>

					<audio rel="st" title="Rock'n'Roll Maniac" src="/media/rocknroll_maniac.m4a"></audio>
					<audio rel="st" title="Gong Operator" src="/media/gong_operator.m4a"></audio>
					<audio rel="st" title="Too Late" src="/media/too_late.m4a"></audio>
					<audio rel="st" title="Rock'n'Roll Rocket" src="/media/rocknroll_rocket.m4a"></audio>
					<audio rel="st" title="Chicago&apos;73" src="/media/chicago_73.m4a"></audio>

					<template id="album-player-template">
						<link rel="stylesheet" href="/css/albumplayer.css">
						<div class="album-player">
							<picture class="album-player__cover"><img /></picture>
							<ol class="album-player__tracklist"></ol>
							<input value="0" min="0" max="1000" type="range" class="album-player__progress">
							<menu class="album-player__controls">
								<li class="album-player__control"><button class="album-player__btn album-player__btn--prev" title="Previous Track">⏮</button></li>
								<li class="album-player__control"><button class="album-player__btn album-player__btn--play" title="Play/Pause">⏵</button></li>
								<li class="album-player__control"><button class="album-player__btn album-player__btn--next" title="Next Track">⏭</button></li>
							</menu>
						</div>
					</template>


                    <figure class="album">
                        <!-- <img src="/img/Gong_Operator_Cover.webp" alt="Gong Operator Album Cover"> -->
                        <figcaption>
                            <p>Listen on</p>
                            <ul class="streamings">
                                <li><?= $this->Html->link('Deezer', '#'); ?></li>
                                <li><?= $this->Html->link('Spotify', '#'); ?></li>
                                <li><?= $this->Html->link('SoundCloud', '#'); ?></li>
                                <li><?= $this->Html->link('YouTube', '#'); ?></li>
                            </ul>
                    </figcaption>
                     </figure> 

                    <?= $this->element('separator', ['stars' => 3]); ?>

                    <h2 class="headline">Live</h2>

                    <?php $cell = $this->cell('UpcomingShows');  echo $cell ?>

                    <?php //echo $this->element('partials/main'); ?>

                    <?= $this->element('separator', ['stars' => 3]); ?>

                    <h2 class="headline">Past Shows</h2>
                    <?php $cell = $this->cell('PastShows');  echo $cell ?>

                    <?= $this->element('separator', ['stars' => 3]); ?>

                    <ul class="social">
                        <li>
                            <a target="_blank" href="https://www.instagram.com/p/Ce_ARXZrgEG/" aria-label="Gong Operator on Instagram">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="1.5" width="24" height="24" color="#000000"><defs><style>.cls-637b8512f95e86b59c57a11c-1{fill:none;stroke:currentColor;stroke-miterlimit:10;}.cls-637b8512f95e86b59c57a11c-2{fill:currentColor;}</style></defs><rect class="cls-637b8512f95e86b59c57a11c-1" x="1.5" y="1.5" width="21" height="21" rx="3.82"></rect><circle class="cls-637b8512f95e86b59c57a11c-1" cx="12" cy="12" r="4.77"></circle><circle class="cls-637b8512f95e86b59c57a11c-2" cx="18.2" cy="5.8" r="1.43"></circle></svg>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" href="https://www.facebook.com/gongoperator" aria-label="Gong Operator on Facebook">
                                <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" stroke-width="1.5" width="24" height="24" color="#000000"><defs><style>.cls-637b8512f95e86b59c57a116-1{fill:none;stroke:currentColor;stroke-miterlimit:10;}</style></defs><path class="cls-637b8512f95e86b59c57a116-1" d="M17.73,6.27V1.5h-1A7.64,7.64,0,0,0,9.14,9.14v.95H6.27v3.82H9.14V22.5h4.77V13.91h2.86V10.09H13.91V9.14a2.86,2.86,0,0,1,2.86-2.87Z"></path></svg>
                            </a>
                        </li>
                    </ul>

                    <?= $this->element('separator', ['stars' => 3]); ?>

                </main>
            </div>
            <footer class="main-footer">
                <?= $this->Html->link(_('Impressum & Datenschutz'), '#legal'); ?>
            </footer>
        </div>
        <!-- Main footer -->
        <?= $this->element('partials/footer') ?>
    </body>

</html>
