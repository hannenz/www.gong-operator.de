<!doctype html>
<html class="no-js" lang="<?= \Cake\Core\Configure::read('App.defaultLocale') ?>">

    <head>
        <?= $this->element('partials/head') ?>
    </head>

    <?= $this->fetch('Rhino') ?>

    <body id="">
        <?= $this->Html->link('#main', _('Skip navigation'), ['class' => 'visually-hidden']); ?>


        <div class="wrapper noisy-background">
            <img class="bg-photo" src="/img/band-2024.webp">
            <div id="shape" class="shape"></div>
            <div class="inner-wrapper">
                <header class="main-header">
                    <h1 class="brand">
                        <div class="brand__image octopus">
                            <?= file_get_contents(WWW_ROOT . '/img/OP_Logo.svg'); ?>
                        </div>
                    </h1>
                </header>

                <main id="main" class="main-content stack stack--full">
					<?= $this->fetch('content'); ?>
                </main>
            </div>
            <footer class="main-footer">
                <?= $this->Html->link(_('Impressum & Datenschutz'), '#legal'); ?>
            </footer>
        </div>
        <!-- Main footer -->
        <?= $this->element('partials/footer') ?>
    </body>

</html>
