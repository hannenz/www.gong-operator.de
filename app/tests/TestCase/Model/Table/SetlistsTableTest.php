<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SetlistsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SetlistsTable Test Case
 */
class SetlistsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SetlistsTable
     */
    protected $Setlists;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected array $fixtures = [
        'app.Setlists',
        'app.Shows',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Setlists') ? [] : ['className' => SetlistsTable::class];
        $this->Setlists = $this->getTableLocator()->get('Setlists', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Setlists);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SetlistsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
