<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Setlists Controller
 *
 * @property \App\Model\Table\SetlistsTable $Setlists
 */
class SetlistsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $query = $this->Setlists->find();
        $setlists = $this->paginate($query);

        $this->set(compact('setlists'));
    }

    /**
     * View method
     *
     * @param string|null $id Setlist id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $setlist = $this->Setlists->get($id, contain: ['Shows']);
        $this->set(compact('setlist'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $setlist = $this->Setlists->newEmptyEntity();
        if ($this->request->is('post')) {
            $setlist = $this->Setlists->patchEntity($setlist, $this->request->getData());
            if ($this->Setlists->save($setlist)) {
                $this->Flash->success(__('The setlist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setlist could not be saved. Please, try again.'));
        }
        $this->set(compact('setlist'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Setlist id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $setlist = $this->Setlists->get($id, contain: []);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $setlist = $this->Setlists->patchEntity($setlist, $this->request->getData());
            if ($this->Setlists->save($setlist)) {
                $this->Flash->success(__('The setlist has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The setlist could not be saved. Please, try again.'));
        }
        $this->set(compact('setlist'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Setlist id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $setlist = $this->Setlists->get($id);
        if ($this->Setlists->delete($setlist)) {
            $this->Flash->success(__('The setlist has been deleted.'));
        } else {
            $this->Flash->error(__('The setlist could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
