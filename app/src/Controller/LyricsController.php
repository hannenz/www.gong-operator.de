<?phP
namespace App\Controller;
use Cake\Http\Client;
use Cake\Utility\Xml;
use Cake\Cache\Cache; 
use Cake\Utility\Text;
use \Parsedown;
use \Exception;


use Rhino\Controller\PagesController as BaseController;

class LyricsController extends BaseController {

    public function beforeFilter(\Cake\Event\EventInterface $event) {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['index', 'view', 'clearCache']);
    }

    public function index() {

        $files = Cache::read('files');
        $isCached = true;
        if ($files === null) {
            $ch = curl_init('https://nextcloud.hannenz.de/remote.php/dav/files/hannenz/Gong%20Operator/Lyrics/md');
            curl_setopt($ch, \CURLOPT_CUSTOMREQUEST, 'PROPFIND');
            curl_setopt($ch, \CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, \CURLOPT_USERPWD, 'hannenz:zDtd8XquCfn3');

            $response = curl_exec($ch);
            if (curl_errno($ch)) {
                throw new Exception($curl_error($ch));
            }

            $_result = Xml::toArray(Xml::build($response));
            if (empty($_result['multistatus']['d:response'])) {
                throw new Exception("Error");
            }

            $files = array_map(function($entry) {
                return urldecode(basename($entry['d:href']));
            }, $_result['multistatus']['d:response']); 
            $isCached = false;
            Cache::write('files', $files);
        }

        $this->set([
            'files' => $files,
            'isCached' => $isCached
        ]);
    }



    public function view($file = null) {
        $key = Text::slug($file);
        $html = Cache::read($key);
        $isCached = true;
        if ($html === null) {
            $client = new Client();
            $data = [];
            $options = [
                'auth' => [
                    'username' => 'hannenz',
                    'password' => 'zDtd8XquCfn3'
                ]
            ];

            $response = $client->get('https://nextcloud.hannenz.de/remote.php/dav/files/hannenz/Gong%20Operator/Lyrics/md/'.$file, $data, $options);
            $markdown = $response->getStringBody();
            $parser = new Parsedown();
            $html = $parser->text($markdown);
            $isCached = false;
            Cache::write($key, $html);
        }
        $this->set([
            'html' => $html,
            'file' => $file,
            'isCached' => $isCached
        ]);
    }


    /**
     * undocumented function
     *
     * @return void
     */
    public function clearCache() {
        Cache::clear();
        $this->redirect($this->referer());
    }
    
}
