<?php
namespace App\View\Cell;
use Cake\View\Cell;

class PastShowsCell extends Cell {
    /**
     * undocumented function
     *
     * @return void
     */
    public function display()
    {
        $shows = $this->fetchTable('Shows')->find('past')->contain(['Locations']);
        $this->set('shows', $shows);
    }
}

