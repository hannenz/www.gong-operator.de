<?php
namespace App\View\Cell;
use Cake\View\Cell;

class UpcomingShowsCell extends Cell {
    /**
     * undocumented function
     *
     * @return void
     */
    public function display()
    {
        $shows = $this->fetchTable('Shows')->find('upcoming')->contain(['Locations']);
        $this->set('shows', $shows);
    }

}
