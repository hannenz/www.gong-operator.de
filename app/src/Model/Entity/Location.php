<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Location Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $description
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $street_address
 * @property string|null $zip
 * @property string|null $city
 *
 * @property \App\Model\Entity\Show[] $shows
 */
class Location extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected array $_accessible = [
        'name' => true,
        'description' => true,
        'latitude' => true,
        'longitude' => true,
        'street_address' => true,
        'zip' => true,
        'city' => true,
        'shows' => true,
    ];
}
