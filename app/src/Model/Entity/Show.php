<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Show Entity
 *
 * @property int $id
 * @property \Cake\I18n\DateTime|null $datetime
 * @property string|null $description
 * @property string|null $location_id
 * @property string|null $setlist_id
 *
 * @property \App\Model\Entity\Location $location
 */
class Show extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected array $_accessible = [
        'datetime' => true,
        'description' => true,
        'location_id' => true,
        'setlist_id' => true,
        'location' => true,
    ];
}
