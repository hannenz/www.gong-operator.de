<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Song Entity
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $lyrics
 * @property string|null $status
 * @property string|null $type
 *
 * @property \App\Model\Entity\Track[] $tracks
 */
class Song extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected array $_accessible = [
        'title' => true,
        'lyrics' => true,
        'status' => true,
        'type' => true,
        'tracks' => true,
    ];
}
