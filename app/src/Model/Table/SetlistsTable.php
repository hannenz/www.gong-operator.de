<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query\SelectQuery;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Setlists Model
 *
 * @property \App\Model\Table\ShowsTable&\Cake\ORM\Association\HasMany $Shows
 *
 * @method \App\Model\Entity\Setlist newEmptyEntity()
 * @method \App\Model\Entity\Setlist newEntity(array $data, array $options = [])
 * @method array<\App\Model\Entity\Setlist> newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Setlist get(mixed $primaryKey, array|string $finder = 'all', \Psr\SimpleCache\CacheInterface|string|null $cache = null, \Closure|string|null $cacheKey = null, mixed ...$args)
 * @method \App\Model\Entity\Setlist findOrCreate($search, ?callable $callback = null, array $options = [])
 * @method \App\Model\Entity\Setlist patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method array<\App\Model\Entity\Setlist> patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Setlist|false save(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method \App\Model\Entity\Setlist saveOrFail(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method iterable<\App\Model\Entity\Setlist>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Setlist>|false saveMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Setlist>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Setlist> saveManyOrFail(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Setlist>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Setlist>|false deleteMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Setlist>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Setlist> deleteManyOrFail(iterable $entities, array $options = [])
 */
class SetlistsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array<string, mixed> $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('setlists');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('Shows', [
            'foreignKey' => 'setlist_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmptyString('title');

        return $validator;
    }
}
