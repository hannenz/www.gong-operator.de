<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query\SelectQuery;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Tracks Model
 *
 * @property \App\Model\Table\SongsTable&\Cake\ORM\Association\BelongsTo $Songs
 *
 * @method \App\Model\Entity\Track newEmptyEntity()
 * @method \App\Model\Entity\Track newEntity(array $data, array $options = [])
 * @method array<\App\Model\Entity\Track> newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Track get(mixed $primaryKey, array|string $finder = 'all', \Psr\SimpleCache\CacheInterface|string|null $cache = null, \Closure|string|null $cacheKey = null, mixed ...$args)
 * @method \App\Model\Entity\Track findOrCreate($search, ?callable $callback = null, array $options = [])
 * @method \App\Model\Entity\Track patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method array<\App\Model\Entity\Track> patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Track|false save(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method \App\Model\Entity\Track saveOrFail(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method iterable<\App\Model\Entity\Track>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Track>|false saveMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Track>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Track> saveManyOrFail(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Track>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Track>|false deleteMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Track>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Track> deleteManyOrFail(iterable $entities, array $options = [])
 */
class TracksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array<string, mixed> $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tracks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Songs', [
            'foreignKey' => 'song_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->scalar('song_id')
            ->maxLength('song_id', 255)
            ->allowEmptyString('song_id');

        $validator
            ->scalar('file')
            ->maxLength('file', 255)
            ->allowEmptyFile('file');

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->allowEmptyString('url');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['song_id'], 'Songs'), ['errorField' => 'song_id']);

        return $rules;
    }
}
