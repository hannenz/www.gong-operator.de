<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query\SelectQuery;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Shows Model
 *
 * @property \App\Model\Table\LocationsTable&\Cake\ORM\Association\BelongsTo $Locations
 *
 * @method \App\Model\Entity\Show newEmptyEntity()
 * @method \App\Model\Entity\Show newEntity(array $data, array $options = [])
 * @method array<\App\Model\Entity\Show> newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Show get(mixed $primaryKey, array|string $finder = 'all', \Psr\SimpleCache\CacheInterface|string|null $cache = null, \Closure|string|null $cacheKey = null, mixed ...$args)
 * @method \App\Model\Entity\Show findOrCreate($search, ?callable $callback = null, array $options = [])
 * @method \App\Model\Entity\Show patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method array<\App\Model\Entity\Show> patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Show|false save(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method \App\Model\Entity\Show saveOrFail(\Cake\Datasource\EntityInterface $entity, array $options = [])
 * @method iterable<\App\Model\Entity\Show>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Show>|false saveMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Show>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Show> saveManyOrFail(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Show>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Show>|false deleteMany(iterable $entities, array $options = [])
 * @method iterable<\App\Model\Entity\Show>|\Cake\Datasource\ResultSetInterface<\App\Model\Entity\Show> deleteManyOrFail(iterable $entities, array $options = [])
 */
class ShowsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array<string, mixed> $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('shows');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
        ]);
        $this->belongsTo('Setlists', [
            'foreignKey' => 'setlist_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->dateTime('datetime')
            ->allowEmptyDateTime('datetime');

        $validator
            ->scalar('description')
            ->allowEmptyString('description');

        $validator
            ->scalar('location_id')
            ->maxLength('location_id', 255)
            ->allowEmptyString('location_id');

        $validator
            ->scalar('setlist_id')
            ->maxLength('setlist_id', 255)
            ->allowEmptyString('setlist_id');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['location_id'], 'Locations'), ['errorField' => 'location_id']);
        $rules->add($rules->existsIn(['setlist_id'], 'Setlists'), ['errorField' => 'setlist_id']);

        return $rules;
    }



    public function findUpcoming(SelectQuery $query) {
        return $query->where(['datetime >' => $query->func()->now()]);
    }

    public function findPast(SelectQuery $query) {
        return $query->where(['datetime <=' => $query->func()->now()]);
    }
}
