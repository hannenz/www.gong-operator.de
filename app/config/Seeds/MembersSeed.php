<?php
declare(strict_types=1);

use Migrations\BaseSeed;

/**
 * Members seed.
 */
class MembersSeed extends BaseSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/migrations/4/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'firstname' => 'Jürgen',
                'lastname' => 'Schleicher',
                'nickname' => 'Joxe',
                'image' => '/media/members',
                'instruments' => '2,3',
            ],
            [
                'id' => 2,
                'firstname' => 'Louis',
                'lastname' => 'Kienle',
                'nickname' => '',
                'image' => '/media/members',
                'instruments' => '0',
            ],
            [
                'id' => 3,
                'firstname' => 'Daniel',
                'lastname' => 'Ott',
                'nickname' => 'Otti',
                'image' => '/media/members',
                'instruments' => '1,3',
            ],
            [
                'id' => 4,
                'firstname' => 'Johannes',
                'lastname' => 'Braun',
                'nickname' => '',
                'image' => '/media/members',
                'instruments' => '0,3',
            ],
        ];

        $table = $this->table('members');
        $table->insert($data)->save();
    }
}
