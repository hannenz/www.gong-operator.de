<?php
declare(strict_types=1);

use Migrations\BaseSeed;

/**
 * Shows seed.
 */
class ShowsSeed extends BaseSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/migrations/4/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'datetime' => '2024-04-27 20:00:00',
                'description' => '',
                'location_id' => '1',
                'setlist_id' => NULL,
            ],
            [
                'id' => 2,
                'datetime' => '2022-12-15 20:00:00',
                'description' => ' Weihnachtspogo mit Rampenbüro',
                'location_id' => '2',
                'setlist_id' => NULL,
            ],
            [
                'id' => 3,
                'datetime' => '2023-02-25 20:00:00',
                'description' => '+ Agent Shit',
                'location_id' => '3',
                'setlist_id' => NULL,
            ],
            [
                'id' => 4,
                'datetime' => '2023-04-29 20:00:00',
                'description' => '',
                'location_id' => '1',
                'setlist_id' => NULL,
            ],
            [
                'id' => 5,
                'datetime' => '2023-05-13 20:00:00',
                'description' => '+ Mental Freeze',
                'location_id' => '4',
                'setlist_id' => NULL,
            ],
            [
                'id' => 6,
                'datetime' => '2023-10-06 20:00:00',
                'description' => '+ Go Go Gazelle',
                'location_id' => '5',
                'setlist_id' => NULL,
            ],
            [
                'id' => 7,
                'datetime' => '2023-12-08 20:00:00',
                'description' => 'Weihnachtspogo mit Rampenbüro und Agent Shit ',
                'location_id' => '2',
                'setlist_id' => NULL,
            ],
        ];

        $table = $this->table('shows');
        $table->insert($data)->save();
    }
}
