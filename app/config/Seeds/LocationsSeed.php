<?php
declare(strict_types=1);

use Migrations\BaseSeed;

/**
 * Locations seed.
 */
class LocationsSeed extends BaseSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/migrations/4/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Cafe Ohne',
                'description' => '',
                'latitude' => '',
                'longitude' => '',
                'street_address' => '',
                'zip' => '',
                'city' => 'Emerkingen',
            ],
            [
                'id' => 2,
                'name' => 'Hemperium',
                'description' => '',
                'latitude' => '',
                'longitude' => '',
                'street_address' => '',
                'zip' => '',
                'city' => 'Ulm',
            ],
            [
                'id' => 3,
                'name' => 'Rössle',
                'description' => '',
                'latitude' => '',
                'longitude' => '',
                'street_address' => '',
                'zip' => '',
                'city' => 'Sontheim',
            ],
            [
                'id' => 4,
                'name' => 'Beetle Bar',
                'description' => '',
                'latitude' => '',
                'longitude' => '',
                'street_address' => '',
                'zip' => '89584',
                'city' => 'Ehingen',
            ],
            [
                'id' => 5,
                'name' => 'Club Schili',
                'description' => '',
                'latitude' => '',
                'longitude' => '',
                'street_address' => '',
                'zip' => '',
                'city' => 'Ulm',
            ],
        ];

        $table = $this->table('locations');
        $table->insert($data)->save();
    }
}
