<?php
declare(strict_types=1);

use Migrations\BaseSeed;

/**
 * Songs seed.
 */
class SongsSeed extends BaseSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/migrations/4/en/seeding.html
     *
     * @return void
     */
    public function run(): void
    {
        $data = [
            [
                'id' => 1,
                'title' => 'Anti',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 2,
                'title' => 'Space Rock Zombies',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 3,
                'title' => 'Something That Detonates',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 4,
                'title' => 'Rock\'n\'Roll Rocket',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 5,
                'title' => 'Too Late',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 6,
                'title' => 'Rock\'n\'Roll Maniac',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 7,
                'title' => 'Search And Destroy',
                'lyrics' => '',
                'status' => '1',
                'type' => '1',
            ],
            [
                'id' => 8,
                'title' => 'I Should Do',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 9,
                'title' => 'Gong Operator',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 10,
                'title' => 'Chicago \'73',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 11,
                'title' => 'Fast Life (In Forbidden Frontiers)',
                'lyrics' => '',
                'status' => '1',
                'type' => '0',
            ],
            [
                'id' => 12,
                'title' => 'Dead Of Me',
                'lyrics' => '',
                'status' => '2',
                'type' => '1',
            ],
        ];

        $table = $this->table('songs');
        $table->insert($data)->save();
    }
}
