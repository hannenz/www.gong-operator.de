<?php
return [
	'App' => [
		'defaultLocale' => env('APP_DEFAULT_LOCALE', 'de_DE'),
	],
    'debug' => filter_var(env('DEBUG', true), FILTER_VALIDATE_BOOLEAN),
    'Security' => [
        'salt' => env('SECURITY_SALT', '65727734db176197b64e473b97d7ab9695df882fd62cfc1cc0d71d84595161cf'),
    ],
    'Datasources' => [
        'default' => [
            'host' => env('DB_HOST'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'database' => env('DB_DATABASE'),
        ],

        'test' => [
            'host' => env('TEST_DB_HOST'),
            'username' => env('TEST_DB_USERNAME'),
            'password' => env('TEST_DB_PASSWORD'),
            'database' => env('TEST_DB_DATABASE'),
            'url' => env('DATABASE_TEST_URL', 'sqlite://127.0.0.1/tmp/tests.sqlite'),
        ],
    ],

    'EmailTransport' => [
        'default' => [
            'host' => 'localhost',
            'port' => 25,
            'username' => null,
            'password' => null,
            'client' => null,
            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        ],
    ],
];
